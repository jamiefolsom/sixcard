class User < ActiveRecord::Base
  has_many :decks
  validates :email, 
    uniqueness: true,
    format: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i
end
