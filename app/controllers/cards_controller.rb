class CardsController < ApplicationController
  before_filter :authenticate

  def new
    @deck = current_user.decks.find(params[:deck_id])
    @card = Card.new
  end

  def create  
    @deck = current_user.decks.find(params[:deck_id])
    @card = @deck.cards.build(params[:card])
    @card.save  
    redirect_to @deck, notice: "Card Saved"
  end

  def show
    @deck = current_user.decks.find(params[:deck_id])
    @card = @deck.cards.find(params[:id])
  end
  
end
