class WelcomeController < ApplicationController
  before_filter :redirect_to_decks_if_signed_in

  def redirect_to_decks_if_signed_in
    redirect_to decks_path, notice:"Welcome to your decks" if signed_in?
  end
end
