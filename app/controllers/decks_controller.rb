class DecksController < ApplicationController
  before_filter :authenticate

  def index
    @decks = current_user.decks
  end
  
  def new
    @deck = Deck.new
  end

  def create
    @deck = current_user.decks.new(params[:deck])
    if @deck.save
      redirect_to decks_path, notice: "Deck Saved"
    else
      render "new"
    end
  end

  def show
    @deck = Deck.find(params[:id])
    @cards = @deck.cards
  end
  
end
