class ApplicationController < ActionController::Base
  protect_from_forgery

  def current_user
    if cookies[:email]
      @current_user ||= User.find_by_email(cookies.signed[:email])
    end
  end
  helper_method :current_user
  
  def sign_in user
    cookies.signed.permanent[:email] = user.email
  end

  def signed_in?
    current_user
  end
  helper_method :signed_in?

  def authenticate
    redirect_to root_path, notice:"You need to be signed in" unless signed_in?
  end
end

