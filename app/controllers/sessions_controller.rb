class SessionsController < ApplicationController
  def new
    
  end


  def create
    if user = User.where(email: params[:session][:email]).first
      sign_in(user)
      redirect_to root_path, notice: "Signed in successfully"
    else
      flash.now[:notice] = "Invalid user"
      render action: "new"
    end
  end
  
  def destroy
    cookies.delete(:email)
    redirect_to root_path, notice: "Signed out successfully"
  end


end
