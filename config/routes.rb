Sixcard::Application.routes.draw do
  root to: "welcome#index"

  # Sign up
  get "sign_up" => "users#new"

  # Sign in/out
  get "sign_in" => "sessions#new"
  post "sign_in" => "sessions#create"
  delete "sign_out" => "sessions#destroy"

  # Following is superceded by the above routes
  # resource :session, only: [:new, :create, :destroy]

  resources :users
  
  resources :decks, only: [:index, :new, :create, :show] do
    resources :cards, only: [:new, :create, :show]
  end
end
